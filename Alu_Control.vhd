----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:45:47 10/24/2018 
-- Design Name: 
-- Module Name:    Alu_Control - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
----------------------------------------------------------------------------------
entity Alu_Control is
	port(
		funct: 				IN		std_logic_vector(5 downto 0);
		AluOp:				IN		std_logic_vector(1 downto 0);
		alu_control_out:	OUT	std_logic_vector(3 downto 0)
	);
end Alu_Control;
----------------------------------------------------------------------------------
architecture Behavioral of Alu_Control is
	signal output: std_logic_vector(3 downto 0);
begin
----------------------------------------------------------------------------------
	process(funct, AluOp, output)
	begin
		case AluOp is
			when "00" =>		--lw&sw
				output <= "0010";			--add
			when "01" =>		--beq
				output <= "0110";			--sub
			when "10" =>		--R type
				case funct is
					when "100100" =>		--and
						output <= "0000";
					when "100101" =>		--or
						output <= "0001";
					when "100000" =>		--add
						output <= "0010";
					when "100001" =>		--addu
						output <= "0011";
					when "100111" =>		--nor
						output <= "0100";
					when "100011" =>		--subu
						output <= "0101";
					when "100010" =>		--sub
						output <= "0110";
					when "101010" =>		--slt
						output <= "0111";
					when "101011" =>		--sltu
						output <= "1000";
					when "000000" =>		--sll shift van reg naar reg
						output <= "1001";
					when "000010" =>		--srl
						output <= "1010";
					when "000011" =>		--sra
						output <= "1011";
		--			when "" =>
		--				output <= x"1100";
		--			when "" =>
		--				output <= x"1101";
		--			when "" =>
		--				output <= x"1110";
		--			when "" =>
		--				output <= x"1111";
					when others => null;
				end case;
				when others => null;
		end case;
	end process;
	alu_control_out <= output;
----------------------------------------------------------------------------------
end Behavioral;

