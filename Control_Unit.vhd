----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:53:34 10/23/2018 
-- Design Name: 
-- Module Name:    Control_Unit - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity Control_Unit is
	port(
		Clk: 					IN 		STD_LOGIC;
		Control_Unit_IN:	IN			STD_LOGIC_VECTOR(5 downto 0);
		RegDst: 				OUT		STD_LOGIC; 
		Jump:					OUT		STD_LOGIC;
		Branch: 				OUT		STD_LOGIC; 
		MemRead: 			OUT		STD_LOGIC; 
		MemToReg: 			OUT		STD_LOGIC; 
		ALUOp: 				OUT		STD_LOGIC_VECTOR(1 downto 0);
		MemWrite: 			OUT		STD_LOGIC;
		ALUSrc: 				OUT		STD_LOGIC; 		
		RegWrite: 			OUT		STD_LOGIC
		);
end Control_Unit;
----------------------------------------------------
architecture Behavioral of Control_Unit is
----------------------------------------------------
begin
	----------------------------------------------------
	-------------------------------------------------------------------------------
	Control:process(Control_Unit_IN)--, Clk)
	begin		--lw en addi ALUOp = 00. sw ook 00. beq is 01.
	--if (rising_edge(Clk)) then
			----------------------------------------------------
				case Control_Unit_IN is 
					when "000000" => -- 0: Alle R-functies (ADD, ADDU, AND, JR, NOR, OR, SLT, SLTU, SLL, SRL, SUB, SUBU)
						RegDst <= '1';
						Jump <= '0';
						Branch <= '0';
						MemRead <= '0';
						MemToReg <= '0';
						ALUOp <= "10";
						MemWrite <= '0';
						ALUSrc <= '0';
						RegWrite <= '1';
					when "000001" => null;-- 1:										
					when "000010" => -- 2: J (Jump)
						RegDst <= '0';
						Jump <= '1';
						Branch <= '0';
						MemRead <= '0';
						MemToReg <= '0';
						ALUOp <= "00";
						MemWrite <= '0';
						ALUSrc <= '0';
						RegWrite <= '0';
					when "000011" => null;-- 3: JAL (Jump And Link)
					when "000100" => -- 4: BEQ (Branch On Equal)
						RegDst <= '0';
						Jump <= '0';
						Branch <= '1';
						MemRead <= '0';
						MemToReg <= '0';
						ALUOp <= "01";
						MemWrite <= '0';
						ALUSrc <= '0';
						RegWrite <= '0';
					when "000101" => null;-- 5: BNE (Branch On Not Equal)
					when "000110" => null;-- 6: 
					when "000111" => null;-- 7: 
					when "001000" =>	-- 8: ADDI (Add Immediate)
						RegDst <= '0';
						Jump <= '0';
						Branch <= '0';
						MemRead <= '0';
						MemToReg <= '0';
						ALUOp <= "00";
						MemWrite <= '0';
						ALUSrc <= '1';
						RegWrite <= '1';
					when "001001" => null;-- 9: ADDIU (Add Imm. Unsigned)
					when "001010" => null;-- A: SLTI (Set Less Than Imm.)
					when "001011" => null;-- B: SLTIU (Set Less Than Imm. Unsigned)
					when "001100" => null;-- C: ANDI (And Immediate)
					when "001101" => null;-- D: ORI (Or Immediate)
					when "001110" => null;-- E: 
					when "001111" => null;-- F: LUI (Load Upper Imm.)
					
					when "010000" => null;-- 10: 
					when "010001" => null;-- 11: 
					when "010010" => null;-- 12:
					when "010011" => null;-- 13:
					when "010100" => null;-- 14:
					when "010101" => null;-- 15:
					when "010110" => null;-- 16:
					when "010111" => null;-- 17:
					when "011000" => null;-- 18:
					when "011001" => null;-- 19:
					when "011010" => null;-- 1A:
					when "011011" => null;-- 1B:
					when "011100" => null;-- 1C:
					when "011101" => null;-- 1D:
					when "011110" => null;-- 1E:
					when "011111" => null;-- 1F:
					
					when "100000" => null;-- 20:
					when "100001" => null;-- 21:
					when "100010" => null;-- 22:
					when "100011" => -- 23: LW (Load Word)
						RegDst <= '0';
						Jump <= '0';
						Branch <= '0';
						MemRead <= '1';
						MemToReg <= '1';
						ALUOp <= "00";
						MemWrite <= '0';
						ALUSrc <= '1';
						RegWrite <= '1';
					when "100100" => null;-- 24: LBU (Load Byte Unsigned)
					when "100101" => null;-- 25: LHU (Load Halfword Unsigned)
					when "100110" => null;-- 26:
					when "100111" => null;-- 27:
					when "101000" => null;-- 28: SB (Store Byte)
					when "101001" => null;-- 29: SH (Store Halfword)
					when "101010" => null;-- 2A:
					when "101011" => -- 2B: SW (Store Word)
						RegDst <= '0';
						Jump <= '0';
						Branch <= '0';
						MemRead <= '0';
						MemToReg <= '0';
						ALUOp <= "00";
						MemWrite <= '1';
						ALUSrc <= '1';
						RegWrite <= '0';
					when "101100" => null;-- 2C:
					when "101101" => null;-- 2D:
					when "101110" => null;-- 2E:
					when "101111" => null;-- 2F:
					
					when "110000" => null;-- 30: LL (Load Linked)
					when "110001" => null;-- 31:
					when "110010" => null;-- 32:
					when "110011" => null;-- 33:
					when "110100" => null;-- 34:
					when "110101" => null;-- 35:
					when "110110" => null;-- 36:
					when "110111" => null;-- 37:
					when "111000" => null;-- 38: SC (Store Conditional)
					when "111001" => null;-- 39:
					when "111010" => null;-- 3A:
					when "111011" => null;-- 3B:
					when "111100" => null;-- 3C:
					when "111101" => null;-- 3D:
					when "111110" => null;-- 3E:
					when "111111" => null;-- 3F:
					when others => null;
			end case;
		--end if;
			----------------------------------------------------
	end process Control;
	-------------------------------------------------------------------------------
	----------------------------------------------------

end Behavioral;

