----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:20:12 10/16/2018 
-- Design Name: 
-- Module Name:    MIPS_PROCESSOR - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity MIPS_PROCESSOR is
		port(	
		clk:					IN 		STD_LOGIC;
		reset:				IN			STD_LOGIC;
		instruction:		OUT		STD_LOGIC_VECTOR(31 downto 0);
		prog_counter:		OUT		STD_LOGIC_VECTOR(31 downto 0)
		);
end MIPS_PROCESSOR;

architecture Behavioral of MIPS_PROCESSOR is
-----------------------------------------------------------------------------------COMPONENTS
COMPONENT ALU
	port(
		a,b : 			in 		std_logic_vector(31 downto 0); -- src1, src2
		alu_control : in 		std_logic_vector(3 downto 0); -- function select
		shamt:			in 		std_logic_vector(4 downto 0); --shift amount
		alu_result: 	out 		std_logic_vector(31 downto 0); -- ALU Output Result
		zero: 			out 		std_logic; -- Zero Flag
		Clk:				in			std_logic
	 );
END COMPONENT;


COMPONENT Alu_Control
	port(
		funct: 				IN		std_logic_vector(5 downto 0);
		AluOp:				IN		std_logic_vector(1 downto 0);
		alu_control_out:	OUT	std_logic_vector(3 downto 0)
	);
END COMPONENT;


COMPONENT Control_Unit
	port(
		Clk: 					IN 		STD_LOGIC;
		Control_Unit_IN:	IN			STD_LOGIC_VECTOR(5 downto 0);
		RegDst: 				OUT		STD_LOGIC; 
		Jump:					OUT		STD_LOGIC;
		Branch: 				OUT		STD_LOGIC; 
		MemRead: 			OUT		STD_LOGIC; 
		MemToReg: 			OUT		STD_LOGIC; 
		ALUOp: 				OUT		STD_LOGIC_VECTOR(1 downto 0);
		MemWrite: 			OUT		STD_LOGIC;
		ALUSrc: 				OUT		STD_LOGIC; 		
		RegWrite: 			OUT		STD_LOGIC
		);
END COMPONENT;


COMPONENT Data_Memory
	port(
		Clk: 			IN 		STD_LOGIC;
		MemWrite: 	IN			STD_LOGIC;
		Address:		IN			STD_LOGIC_VECTOR(31 downto 0);
		WriteData:	IN			STD_LOGIC_VECTOR(31 downto 0);
		ReadData:	OUT		STD_LOGIC_VECTOR(31 downto 0);
		MemRead:		IN			STD_LOGIC
	);
END COMPONENT;


COMPONENT IMem
	port(
		PC: 				IN		STD_LOGIC_VECTOR(31 downto 0);
		Instruction:	OUT	STD_LOGIC_VECTOR(31 downto 0)	
	);
END COMPONENT;


COMPONENT MUX_2
	generic(
		width: INTEGER	:=8
	);
	port(
		INPUT_0: 	IN		STD_LOGIC_VECTOR(width-1 downto 0);
		INPUT_1:		IN		STD_LOGIC_VECTOR(width-1 downto 0);
		OUTPUT:		OUT	STD_LOGIC_VECTOR(width-1 downto 0);
		SELECTOR:	IN		STD_LOGIC
	);	
END COMPONENT;


COMPONENT MUX_3
	generic(
		width: INTEGER	:=8
	);
	port(
		INPUT_0: 	IN		STD_LOGIC_VECTOR(width-1 downto 0);
		INPUT_1:		IN		STD_LOGIC_VECTOR(width-1 downto 0);
		INPUT_2:		IN		STD_LOGIC_VECTOR(width-1 downto 0);
		OUTPUT:		OUT	STD_LOGIC_VECTOR(width-1 downto 0);
		SELECTOR:	IN		STD_LOGIC
	);	
END COMPONENT;


COMPONENT PC
	port(
		Clk:	IN	STD_LOGIC;
		Reset:	IN	STD_LOGIC; 		
		PC_IN:	IN	STD_LOGIC_VECTOR(31 downto 0); 	
		PC_OUT:	OUT	STD_LOGIC_VECTOR(31 downto 0)	
	); 
END COMPONENT;


COMPONENT PCA
	port(
		PCA_IN:	IN	STD_LOGIC_VECTOR(31 downto 0); 	
		PCA_OUT:	OUT	STD_LOGIC_VECTOR(31 downto 0)	
	); 
END COMPONENT;


COMPONENT ShiftLeft
	generic(
		N: INTEGER	:=1;
		width: INTEGER :=8
	); 
	port(
		Input:	IN		STD_LOGIC_VECTOR(width-1 downto 0);
		Output:	OUT	STD_LOGIC_VECTOR(width-1 downto 0)
		);
END COMPONENT;


COMPONENT RegisterFile
	port(		
		Read_Register_1:	IN		STD_LOGIC_VECTOR(4 downto 0);
		Read_Register_2:	IN		STD_LOGIC_VECTOR(4 downto 0);
		Write_Register:	IN		STD_LOGIC_VECTOR(4 downto 0);		
		RegWrite:			IN		STD_LOGIC;		
		write_data:			IN		STD_LOGIC_VECTOR(31 downto 0);		
		---- read data
		read_data_1: 		OUT 	STD_LOGIC_VECTOR(31 downto 0);
		read_data_2: 		OUT 	STD_LOGIC_VECTOR(31 downto 0);
		Clk:					IN 	STD_LOGIC
	);	
END COMPONENT;


COMPONENT Sign_Extend
	port (
		Input: 				IN 	STD_LOGIC_VECTOR(15 downto 0);
		Output: 				OUT 	STD_LOGIC_VECTOR(31 downto 0)
	);
END COMPONENT;

COMPONENT JumpAdrBuff
	port(
		Instr26:	IN	STD_LOGIC_VECTOR(25 downto 0);
		Pc4In : IN STD_LOGIC_VECTOR(3 downto 0);
		JumpAdr:	OUT	STD_LOGIC_VECTOR(31 downto 0)	
	); 
END COMPONENT;
-----------------------------------------------------------------------------------SIGNALS

	signal instruction_signal:STD_LOGIC_VECTOR(31 downto 0);
	signal program_counter:STD_LOGIC_VECTOR(31 downto 0);
	signal program_counter4: STD_LOGIC_VECTOR(31 downto 0);
	signal beq_signal: STD_LOGIC;
	
	signal RegFile_WriteReg: STD_LOGIC_VECTOR(4 downto 0);
	signal RegFile_WriteData: STD_LOGIC_VECTOR(31 downto 0);
	signal RegFile_ReadData1: STD_LOGIC_VECTOR(31 downto 0);
	signal RegFile_ReadData2: STD_LOGIC_VECTOR(31 downto 0);
	
	signal ControlUnit_RegDst: STD_LOGIC;
	signal ControlUnit_RegWrite: STD_LOGIC;
	signal ControlUnit_ALUSrc: STD_LOGIC;
	signal ControlUnit_AluOp: STD_LOGIC_VECTOR(1 downto 0);
	signal ControlUnit_Branch: STD_LOGIC;
	signal ControlUnit_Jump: STD_LOGIC;
	signal ControlUnit_MemRead: STD_LOGIC;
	signal ControlUnit_MemToReg: STD_LOGIC;
	signal ControlUnit_MemWrite: STD_LOGIC;
	
	signal AluControl_Out: STD_LOGIC_VECTOR(3 downto 0);
	
	signal AluMux_In1: STD_LOGIC_VECTOR(31 downto 0);
	signal Alu_b: STD_LOGIC_VECTOR(31 downto 0);
	signal Alu_out: STD_LOGIC_VECTOR(31 downto 0);
	signal Alu_ZeroFlag: STD_LOGIC;
	
	signal DataMemory_ReadData: STD_LOGIC_VECTOR(31 downto 0);
	
	signal JumpMux_Input1: STD_LOGIC_VECTOR(31 downto 0);
	signal JumpMux_Input0: STD_LOGIC_VECTOR(31 downto 0);
	signal JumpMux_Output: STD_LOGIC_VECTOR(31 downto 0);
	signal BranchShiftOut: STD_LOGIC_VECTOR(31 downto 0);
	signal BranchMux_Input1: STD_LOGIC_VECTOR(31 downto 0);
	
	--signal Instruction28: STD_LOGIC_VECTOR(25 downto 0);
-----------------------------------------------------------------------------------BEGIN
begin
-------------------------------------------------------------------------
	PCounter: PC port map (
		Clk => clk,
		Reset => reset,
		PC_OUT => program_counter,
		PC_IN => JumpMux_Output
	);
-------------------------------------------------------------------------
	InstructionMem: IMem port map (
		PC => program_counter,
		Instruction => instruction_signal
	);
-------------------------------------------------------------------------
	PCAdder: PCA port map ( --LINKED with PC
		PCA_IN => program_counter,
		PCA_OUT => program_counter4
	);
-------------------------------------------------------------------------
	RegFileInMux: MUX_2 
	generic map (
		Width => 5
	)
	port map(
		INPUT_0 => instruction_signal(20 downto 16),
		INPUT_1 => instruction_signal(15 downto 11),
		OUTPUT => RegFile_WriteReg,
		SELECTOR => ControlUnit_RegDst
	);
-------------------------------------------------------------------------
	RegFile: RegisterFile port map (
		Read_Register_1 => instruction_signal(25 downto 21),
		Read_Register_2 => instruction_signal(20 downto 16),
		Write_Register => RegFile_WriteReg,		
		RegWrite => ControlUnit_RegWrite,	
		write_data => RegFile_WriteData,		
		read_data_1 => RegFile_ReadData1,
		read_data_2 => RegFile_ReadData2,
		Clk => clk
	);
-------------------------------------------------------------------------
	AluSignExt: Sign_Extend
	port map (
		Input => instruction_signal(15 downto 0),
		Output => AluMux_In1
	);
-------------------------------------------------------------------------
	AluMux: MUX_2
	generic map (
		Width => 32
	)
	port map (
		INPUT_0 => RegFile_ReadData2,
		INPUT_1 => AluMux_In1,
		OUTPUT => Alu_b,
		SELECTOR => ControlUnit_ALUSrc
	);
-------------------------------------------------------------------------
	AluEntity: ALU port map (
		a => RegFile_ReadData1,
		b => Alu_b,
		alu_control => AluControl_Out,
		shamt => instruction_signal(10 downto 6),
		alu_result => Alu_out,
		zero => Alu_ZeroFlag,
		Clk => clk
	);
-------------------------------------------------------------------------
	AluControl: Alu_Control port map(
		funct => instruction_signal(5 downto 0),
		AluOp => ControlUnit_AluOp,
		alu_control_out => AluControl_Out
	);
-------------------------------------------------------------------------
	ControlUnit: Control_Unit port map (
		Clk => clk,
		Control_Unit_IN => instruction_signal(31 downto 26),
		RegDst => ControlUnit_RegDst ,
		Jump => ControlUnit_Jump,
		Branch => ControlUnit_Branch, 
		MemRead => ControlUnit_MemRead,
		MemToReg => ControlUnit_MemToReg,
		ALUOp => ControlUnit_AluOp,
		MemWrite => ControlUnit_MemWrite,
		ALUSrc => ControlUnit_ALUSrc,
		RegWrite => ControlUnit_RegWrite
	);
-------------------------------------------------------------------------
	DataMemory: Data_Memory port map (
		Clk => clk,
		MemWrite => ControlUnit_MemWrite,
		Address => Alu_out,
		WriteData => RegFile_ReadData2,
		ReadData => DataMemory_ReadData,
		MemRead => ControlUnit_MemRead		
	);
-------------------------------------------------------------------------
	MemToRegMux: MUX_2
	generic map(
		Width => 32
	)
	port map(
		INPUT_0 => Alu_out,
		INPUT_1 => DataMemory_ReadData,
		OUTPUT => RegFile_WriteData,
		SELECTOR => ControlUnit_MemToReg
	);
-------------------------------------------------------------------------
--	JumpShift: ShiftLeft
--	generic map (
--		width => 26,
--		N => 2
--	)
--	port map (
--		Input => instruction_signal(25 downto 0),
--		Output => Instruction28
--	);
-------------------------------------------------------------------------
	BranchShift: ShiftLeft
	generic map (
		width => 32,
		N => 2
	)
	port map (
		Input => AluMux_In1,
		Output => BranchShiftOut
	);
-------------------------------------------------------------------------
	BranchMux: MUX_2
	generic map (
		Width => 32
	)
	port map (
		INPUT_0 => program_counter4,
		INPUT_1 => BranchMux_Input1,
		OUTPUT => JumpMux_Input0,
		SELECTOR => beq_signal
	);
-------------------------------------------------------------------------
	JumpMux: MUX_2
	generic map (
		Width => 32
	)
	port map (
		INPUT_0 => JumpMux_Input0,
		INPUT_1 => JumpMux_Input1,
		OUTPUT => JumpMux_Output,
		SELECTOR => ControlUnit_Jump
	);
-------------------------------------------------------------------------
	JmpAdrBuff: JumpAdrBuff
	port map(
		Instr26 => instruction_signal(25 downto 0),
		Pc4In => program_counter4(31 downto 28),
		JumpAdr => JumpMux_Input1
	);

	prog_counter <= JumpMux_Output;
	--instruction_signal <= Instruction;
	--JumpMux_Input1(31 downto 28) <= program_counter4(31 downto 28);
	BranchMux_Input1 <= STD_LOGIC_VECTOR(unsigned(BranchShiftOut) + unsigned(program_counter4));
	beq_signal <= Alu_ZeroFlag and ControlUnit_Branch;
	instruction <= instruction_signal;

end Behavioral;
