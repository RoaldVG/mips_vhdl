----------------------------------------------------------------------------------
-- Company: 
-- Engineer:  Segers Laurent
-- 
-- Create Date:    21:04:01 01/18/2013 
-- Design Name: 
-- Module Name:    IMem - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

----------------------------------------------------------------------------------
entity IMem is
port(
	PC: 				IN		STD_LOGIC_VECTOR(31 downto 0);
	Instruction:	OUT	STD_LOGIC_VECTOR(31 downto 0)	
);
end IMem;
----------------------------------------------------------------------------------
architecture Behavioral of IMem is	
begin
	-------------------------------------------------------------------------------
	MemoryPC:process(PC)
		subtype 		word 		is STD_LOGIC_VECTOR(31 downto 0);
		type 			memory 	is array(0 to 7) of word;
		variable 	myMem:	memory	:=	
		(
		"00100000000000010000000000000001", --addi reg1=zero+1
		"00100000000000100000000000000001", --addi reg2=zero+1
		"00001000000000000000000000000110", --jump
		"00000000000000000000000000000000", -- 
		"00000000000000000000000000000000", --
		"00010000001000100000000000000001", --beq
		"00000000000000000000000000000000",	--
		"00100000000000010000000000000011"  --
		);
		--(X"20090001", X"200afffb", X"012a5820", X"ac0b0000",
		-- X"8c0c0000", X"01890018", X"00004810", X"00005012");		
	begin		
		if (to_integer(unsigned(PC(31 downto 2))) < 8) then
			Instruction<=myMem(to_integer(unsigned(PC(31 downto 2)))); -- truncation --> gedeeld door 4, vhdl adressen zijn per 4 bytes, dus adres moet die sprongen maken
		end if;
	end process MemoryPC;
	-------------------------------------------------------------------------------	
end Behavioral;

