----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    10:42:40 10/23/2018 
-- Design Name: 
-- Module Name:    Data_Memory - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
----------------------------------------------------
entity Data_Memory is
	port(
		Clk: 			IN 		STD_LOGIC;
		MemWrite: 	IN			STD_LOGIC;
		Address:		IN			STD_LOGIC_VECTOR(31 downto 0);
		WriteData:	IN			STD_LOGIC_VECTOR(31 downto 0);
		ReadData:	OUT		STD_LOGIC_VECTOR(31 downto 0);
		MemRead:		IN			STD_LOGIC
	);
end Data_Memory;
----------------------------------------------------
architecture Behavioral of Data_Memory is
----------------------------------------------------
begin
	----------------------------------------------------
	-------------------------------------------------------------------------------
	Memory:process(Address, MemWrite, MemRead, WriteData)--, Clk)
		subtype 		word 		is STD_LOGIC_VECTOR(31 downto 0); 	-- word
		type 			memory 	is array(0 to 127) of word; 			-- array van 8 words
		variable 	ram:	memory :=	(others=>(others=>'0')); --Alles op 0 zetten	
	begin		
			----------------------------------------------------
				-- Read Data altijd doorgeven (onafhankelijk vd clk)
				if(to_integer(unsigned(Address)) < 128) then --controle, want memory heeft maar 128 words.
					if(MemRead = '1') then
						ReadData <= ram(to_integer(unsigned(Address)));
						--ReadData <= ram(to_integer(unsigned(ram_addr))) when (Mem_Read='1') else x"0000"; 
					elsif (MemWrite = '1') then 
						ram(to_integer(unsigned(Address))) := WriteData; --WriteData schrijven naar memory
					end if;
				else 
					ReadData <= (others=>'0'); --ReadData op 0 zetten als address te groot is
				end if;
				
				-- Data schrijven enkel op clk-flank en als writemem hoog staat
				--if (rising_edge(Clk)) then
--					if(MemWrite = '1') then
--						if(to_integer(unsigned(Address)) < 128) then
--							myMem(to_integer(unsigned(Address))) := WriteData; --WriteData schrijven naar memory
--						end if;
--					end if;
				--end if;
			----------------------------------------------------
	end process Memory;
	-------------------------------------------------------------------------------
	----------------------------------------------------
end Behavioral;

