----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    15:28:32 10/24/2018 
-- Design Name: 
-- Module Name:    Sign_Extend - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;
----------------------------------------------------------------------------------
entity Sign_Extend is
	port (
		Input: 	IN		std_logic_vector(15 downto 0);
		Output:	OUT	std_logic_vector(31 downto 0)
	);
end Sign_Extend;
----------------------------------------------------------------------------------
architecture Behavioral of Sign_Extend is
	signal sign: std_logic;
begin
----------------------------------------------------------------------------------
	Output(15 downto 0) <= Input(15 downto 0);
	sign <= Input(15);
	Output(31 downto 16) <= (others=>sign);
----------------------------------------------------------------------------------
end Behavioral;

