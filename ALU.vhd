----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    11:02:23 10/23/2018 
-- Design Name: 
-- Module Name:    ALU - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_signed.ALL;
use IEEE.NUMERIC_STD.ALL;
----------------------------------------------------------------------------------
entity ALU is
	port(
	 a,b : 			in 		std_logic_vector(31 downto 0); -- src1, src2
	 alu_control : in 		std_logic_vector(3 downto 0); -- function select
	 shamt:			in 		std_logic_vector(4 downto 0); --shift amount
	 alu_result: 	out 		std_logic_vector(31 downto 0); -- ALU Output Result
	 zero: 			out 		std_logic; -- Zero Flag
	 Clk:				in			std_logic
	 );
end ALU;
----------------------------------------------------------------------------------
architecture Behavioral of ALU is
	signal result: std_logic_vector(31 downto 0);
begin
----------------------------------------------------------------------------------
	ALU:process(alu_control,a,b)--, Clk)
	begin
		--if (rising_edge(Clk)) then

			case alu_control is
				when "0000" => 	--and
					result <= a and b;
				when "0001" =>		--or
					result <= a or b; 
				when "0010" =>		--add				
					result <= std_logic_vector(signed(a) + signed(b)); 
				when "0011" =>		--addu
					result <= std_logic_vector(unsigned(a) + unsigned(b)); 
				when "0100" =>		--nor
					result <= a nor b; 
				when "0101" =>		--subu
					result <= std_logic_vector(unsigned(a) - unsigned(b));
				when "0110" =>		--sub
					result <= std_logic_vector(signed(a) - signed(b)); 
				when "0111" =>		--slt
					if (signed(a)<signed(b)) then	
						result(31 downto 1) <= (others=>'0');
						result(0) <= '1';
					else 
						result <= (others=>'0');
					end if;
				when "1000" =>		--sltu
					if (unsigned(a) < unsigned(b)) then	
						result(31 downto 1) <= (others=>'0');
						result(0) <= '1';
					else 
						result <= (others=>'0');
					end if;
				when "1001" => 	--sll
					--result(31) <= b(31); --sign behouden moet niet
					result(31 downto to_integer(unsigned(shamt))) <= b(31-to_integer(unsigned(shamt)) downto 0);
					result(to_integer(unsigned(shamt))-1 downto 0) <= (others=>'0');
				when "1010" => 	--slr
					result(31-to_integer(unsigned(shamt)) downto 0) <= b(31 downto to_integer(unsigned(shamt)));
					if(to_integer(unsigned(shamt)) /= 0) then
						result(31 downto 32-to_integer(unsigned(shamt))) <= (others=>'0');
					end if;
				when "1011" => 	--sra 
					result(31-to_integer(unsigned(shamt)) downto 0) <= b(31 downto to_integer(unsigned(shamt)));
					if(to_integer(unsigned(shamt)) /= 0) then
						result(31 downto 32-to_integer(unsigned(shamt))) <= (others=>b(31));
					end if;
				when others => null;
			end case;
				

		--end if;

	end process ALU;
	-------------------------------------------------------------------------------

	Zero_ALU_Output:process(result)
	begin
		if (result="00000000000000000000000000000000") then
				zero <= '1';
		else
				zero <= '0';
		end if;
	end process Zero_ALU_Output;
-------------------------------------------------------------------------------
			
	alu_result <= result;
----------------------------------------------------------------------------------
end Behavioral;

