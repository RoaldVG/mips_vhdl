----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    14:37:24 12/11/2018 
-- Design Name: 
-- Module Name:    JumpAdrBuff - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity JumpAdrBuff is
port(
	Instr26:	IN	STD_LOGIC_VECTOR(25 downto 0);
	Pc4In : IN STD_LOGIC_VECTOR(3 downto 0);
	JumpAdr:	OUT	STD_LOGIC_VECTOR(31 downto 0)	
); 
end JumpAdrBuff;

architecture Behavioral of JumpAdrBuff is

begin

	process(Instr26,Pc4In)
	begin
		JumpAdr(1 downto 0) <= (others=>'0');
		JumpAdr(27 downto 2) <= Instr26;
		JumpAdr(31 downto 28) <= Pc4In;
	end process;

end Behavioral;

