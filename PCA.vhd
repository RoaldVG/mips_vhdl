----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date:    09:59:18 11/06/2018 
-- Design Name: 
-- Module Name:    PCA - Behavioral 
-- Project Name: 
-- Target Devices: 
-- Tool versions: 
-- Description: 
--
-- Dependencies: 
--
-- Revision: 
-- Revision 0.01 - File Created
-- Additional Comments: 
--
--------------------------------------------------------------------------------------------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.STD_LOGIC_ARITH.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;
----------------------------------------------------------------------------------
entity PCA is
port(
	PCA_IN:	IN	STD_LOGIC_VECTOR(31 downto 0); 	
	PCA_OUT:	OUT	STD_LOGIC_VECTOR(31 downto 0)	
); 
end PCA;

architecture Behavioral of PCA is
	
begin
	PCadder:process(PCA_IN)
	begin
			PCA_OUT <= PCA_IN + 4;
	end process PCadder;

end Behavioral;

